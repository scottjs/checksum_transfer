package main

import (
	"encoding/gob"
	"hash/crc32"
	"io"
	"net"
	"os"
	"fmt"
)

type Client struct {
	TransferInfo
	ChunkSize  int
	connection net.Conn
	enc        *gob.Encoder
}

func (c *Client) initConnection() error {
	c.enc = gob.NewEncoder(c.connection)
	err := c.enc.Encode(c.TransferInfo)
	if err != nil {
		return err
	}
	return nil
}

func (c *Client) Connect(address string) error {
	var err error
	c.connection, err = net.Dial("tcp", address)
	if err != nil {
		return err
	}
	err = c.initConnection()
	if err != nil {
		return err
	}
	return nil
}

func (c *Client) Send(f *os.File) error {
	defer c.connection.Close()
	segment := make([]byte, c.ChunkSize)
	response := make([]byte, 1, 1)
	for {
		br, err := f.Read(segment)
		if err != nil && err != io.EOF {
			return err
		} else if br == 0 && err == io.EOF {
			return nil
		} else if br < len(segment) {
			segment = segment[:br]
		}
		chk := FileChunk{crc32.ChecksumIEEE(segment), segment}
		rcvok := false
		//Loop sending until receive OK
		for rcvok == false {
			err = c.enc.Encode(chk)
			fmt.Print(".")
			if err != nil {
				return err
			}
			_, err = c.connection.Read(response)
			if err != nil {
				return err
			}
			if response[0] == OK {
				rcvok = true
			}
		}

	}
	return nil
}

type TransferInfo struct {
	FileName string
	//ServerPW string
}
