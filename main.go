package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"time"
)

const (
	OK = 0
	RT = 1
)

type FileChunk struct {
	Checksum  uint32
	Chunkdata []byte
}

func (f FileChunk) CheckEqual(chksum uint32) bool {
	if f.Checksum == chksum {
		return true
	} else {
		return false
	}
}

func main() {
	//Set up flags
	//Parse flags
	//Make sure no conflicting flags
	//Launch Server or Client

	var (
		isServer   bool
		listenPort string
		destFolder string
		filePath   string
		address    string
		chunkSize  int
	)
	flag.BoolVar(&isServer, "S", false, "Act as server (default False)")
	flag.StringVar(&listenPort, "n", "", "Port to listen on")
	flag.StringVar(&destFolder, "d", "", "Folder to which to send files")
	flag.StringVar(&filePath, "f", "", "File to send")
	flag.StringVar(&address, "a", "", "Destination server address")
	flag.IntVar(&chunkSize, "s", 1000000, "Size in bytes to split file")
	flag.Parse()

	if isServer {
		if len(listenPort) > 0 && len(destFolder) > 0 {
			doServer(listenPort, destFolder)
		} else {
			fmt.Println("Missing required server parameters")
			flag.PrintDefaults()
		}
	} else {
		if len(filePath) > 0 && len(address) > 0 {
			doClient(address, filePath, chunkSize)
		} else {
			fmt.Println("Missing required client parameters")
			flag.PrintDefaults()
		}
	}

}

func doServer(port, dest string) {
	serv := Server{}
	serv.Port = port
	serv.Filedest = dest
	fmt.Println("Starting server on port", port,)
	err := serv.Listen()
	if err != nil {
		fmt.Println(err)
		return
	}
}

func doClient(address, file string, chunk int) {
	client := Client{}
	client.ChunkSize = chunk
	client.FileName = filepath.Base(file)
	fmt.Println("Filename: ", client.FileName)
	f, err := os.Open(file)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()
	err = client.Connect(address)
	if err != nil {
		fmt.Println(err)
		return
	}
	t1 := time.Now()
	client.Send(f)
	fmt.Println("\nTransfer completed in", time.Since(t1))
}
