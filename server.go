package main

import (
	"encoding/gob"
	"fmt"
	"hash/crc32"
	"io"
	"net"
	"os"
	"path"
)

type Server struct {
	Port       string
	Filedest   string
	resp       []byte
	connection net.Conn
	filename   string
	dec        *gob.Decoder
}

func (s *Server) Listen() error {
	l, err := net.Listen("tcp", ":"+s.Port)
	if err != nil {
		return err
	}
	defer l.Close()
	s.resp = make([]byte, 1, 1)
	for {
		fmt.Println("Listening...")
		s.connection, err = l.Accept()
		if err != nil {
			return err
		}
		err = s.initConnection()
		if err != nil {
			return err
		}
		err = s.receiveData()
		if err != nil {
			return err
		}
		fmt.Println("Closing connection")
		s.connection.Close()
	}
	return nil
}

func (s *Server) initConnection() error {
	var ti TransferInfo
	s.dec = gob.NewDecoder(s.connection)
	fmt.Println("Initating connection")
	err := s.dec.Decode(&ti)
	if err != nil {
		return err
	}
	fmt.Println("Filename: ", ti.FileName)
	s.filename = ti.FileName
	return nil
}

func (s *Server) receiveData() error {
	var chunk FileChunk
	fmt.Println("Receiving data")
	fmt.Println(path.Join(s.Filedest, s.filename))
	file, err := os.Create(path.Join(s.Filedest, s.filename))
	if err != nil {
		return err
	}
	defer file.Close()
	for {
		err = s.dec.Decode(&chunk)
		if err != nil && err != io.EOF {
			return err
		} else if err == io.EOF {
			fmt.Println("Transfer successful")
			return nil
		}
		if chunk.CheckEqual(crc32.ChecksumIEEE(chunk.Chunkdata)) {
			file.Write(chunk.Chunkdata)
			s.sendOK()
		} else {
			s.sendRT()
		}
	}
}

func (s *Server) sendOK() error {
	s.resp[0] = OK
	_, err := s.connection.Write(s.resp)
	if err != nil {
		return err
	}
	return nil
}

func (s *Server) sendRT() error {
	s.resp[0] = RT
	_, err := s.connection.Write(s.resp)
	if err != nil {
		return err
	}
	return nil
}
